#!/bin/bash
#
# Kiet To - kwrazi@gmail.com
#

# Note:
# If you change the location and or name of the playlist file. Please
# edit the configuration in hourly_chime.py
#

SCRIPTDIR="${HOME}/Scripts"
SND_DST="${HOME}/Sounds"
SND_SRC="Sounds/soundfxcenter.com"
SCRIPT="hourly_chime.py"
PLAYLIST="hourlyplaylist.txt"

function download_sound() {
	SFILE="big_bang_theory.url"
	if [ -f "${SND_SRC}/${SFILE}" ]; then
		pushd "${SND_SRC}"
		echo "Downloading sound files..."
		wget -c -i "${SFILE}"
		popd
	fi
}

function install_sound() {
	if [ ! -d "${SND_DST}" ]; then
		echo "Creating sound directory ${SND_DST}..."
		mkdir -p "${SND_DST}"
	fi
	echo "Copying sound files..."
	/bin/cp -Ruv "${SND_SRC}" "${SND_DST}"
	
	PL="${SND_DST}/${PLAYLIST}"
	if grep -q soundfxcenter "${PL}"; then
		echo "Playlist has sound files..."
	else
		echo "Generating playlist ${PL}..."
		/bin/ls -1 "${SND_DST}/soundfxcenter.com/"*.{mp3,ogg,wav} >> "${PL}"
	fi
}

function install_script() {
	if [ -d "${SCRIPTDIR}" ]; then
		echo "Installing script..."
		/bin/cp -vu "${SCRIPT}" "${SCRIPTDIR}"
	else
		echo "Directory ${SCRIPTDIR} not found. Aborting."
		exit 1
	fi
}

function install_crontab() {
	if crontab -l | grep -q "${SCRIPT}"; then
		echo "Script already installed. Skipping."
	else
		echo "Adding script to crontab..."
		#crontab -l; echo "0 * * * * /usr/bin/python ${SCRIPTDIR}/${SCRIPT}";
		(crontab -l; echo "0 * * * * /usr/bin/python ${SCRIPTDIR}/${SCRIPT}";) | crontab
	fi
}

function test_script() {
	echo "Testing script. Executing script..."
	/usr/bin/python "${SCRIPTDIR}/${SCRIPT}"
	echo "You should have heard a soundclip being played and seen a notification popup."
}

download_sound
install_sound
install_script
install_crontab
test_script 

echo "done."

