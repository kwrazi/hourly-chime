#!/usr/bin/python
#
# Kiet To
# 26th April 2013
# modified:
# 13th November 2013

# Set this up by adding this line to the user (non-root) crontab
# 0 * * * * /usr/bin/python <fullpath>/hourly_chime.py

from datetime import datetime
import subprocess
import random
import os
import getpass
import pwd
import sys

def owner(pid):
	'''Return username of UID of process pid'''
	UID = 1
	EUID = 2
	for ln in open('/proc/%d/status' % pid):
		if ln.startswith('Uid:'):
			uid = int(ln.split()[UID])
			return pwd.getpwuid(uid).pw_name

def copy_environ(pid,varlist=None):
	'''copy a list of environment variables from another process'''
	env_dict = dict(z.split('=',1) for z in [y for x in open('/proc/%d/environ' % pid) for y in x.split('\0') if y != ''])
	if not varlist:
		varlist = env_dict.keys()
	for var in varlist:
		if var in env_dict:
			os.environ[var] = env_dict[var]
		else:
			print('Error: cannot find environment variable %s in process %d' % (var,pid))
			sys.exit()

def set_gnome_environ(pid):
	# minimum set of environment variables that need to be set
	gnome_env=['XDG_RUNTIME_DIR','DISPLAY','DBUS_SESSION_BUS_ADDRESS','XAUTHORITY']
	copy_environ(pid,gnome_env)

def check_gnome():
	'''Test if gnome-session is running and matches script process owner'''
	# Note: assumes only one gnome-session is running at a time on linux system
	who_am_i = getpass.getuser()
	ps_data = [(int(p), c) for p, c in [x.rstrip('\n').split(' ', 1) for x in os.popen('ps h -eo pid:1,command')]]
	for p,c in ps_data:
		cmd=c.split(' ', 1)[0]
		if "gnome-session" in cmd:
			gnome_pid=p
			break
	if 'gnome_pid' not in locals():
		print('No gnome-session found. Aborting.')
		sys.exit()
	gnome_owner=owner(gnome_pid)
	if who_am_i != gnome_owner:
		print('Script owner does not own desktop. Aborting.')
		sys.exit()
	set_gnome_environ(gnome_pid)

def notify_time():
	
	GObject.set_prgname('hourly_chime')
	GObject.set_application_name('Hourly Chimer')

	if not Notify.is_initted():
		if not Notify.init(DefaultConfig['APPLICATION']):
			raise ImportError("Couldn't initialize libnotify")

	now = datetime.now()

	m = Notify.Notification.new(
		DefaultConfig['TITLE'],
		str(now),
		DefaultConfig['ICON'])

	m.set_timeout(DefaultConfig['TIMEOUT'] * 1000)
	m.set_urgency(DefaultConfig['URGENCY'])

	res = m.show()
	#try:
	#   res = m.show()
	#except GError:
	#   pass

	return m

def play_audio():

	playlistfile = os.path.expandvars(DefaultConfig['SOUNDDIR']) + "/" + DefaultConfig['PLAYLIST']
	lines = open(playlistfile).read().splitlines()
	audiofile = random.choice(lines)

        # depend on the type of audio output system you have
	#cmd = ['play','-q',audiofile,'-t','oss','norm','-3']
	#cmd = ['play','-q',audiofile,'-t','alsa','norm','-3']
	cmd = ['play','-q',audiofile,'-t','pulseaudio','norm','-3']
	process = subprocess.Popen(cmd)
	out,err = process.communicate()

if __name__ == '__main__':
	import time
	
	check_gnome()
	
	# certain gnome environment variables must be set before the 
	# gnome python modules are loaded. When this script is executed in 
	# crond, only a bare number of environment variables are set. 
	# Hence, check_gnome() must be executed before hand.
	
	from gi.repository import Notify, GObject, Gtk
	
	DefaultConfig = {
		'SOUNDDIR'    : "${HOME}/Sounds",
		'PLAYLIST'    : 'hourlyplaylist.txt',
		'TIMEOUT'     : 5,
		'ICON'        : "appointment-soon",
		'APPLICATION' : "chimes",
		'TITLE'       : "Another hour has passed.",
		'URGENCY'     : Notify.Urgency.LOW
	}
	
	msg = notify_time()
	play_audio()
	time.sleep(5)
	msg.close()
